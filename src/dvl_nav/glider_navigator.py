"""The ros wrapper for DVL Navigation"""

# import traceback

# from ds_core_msgs.msg import RawData
from sentinel_ctd_msgs.msg import CTDData
from ds_sensor_msgs.msg import Ahrs
from ds_sensor_msgs.msg import RdiPD0

from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Vector3


# from geometry_msgs import Twist
# from dvl_nav.PathfinderDVL import PathfinderDVL
# from dvl_nav.PathfinderTimeSeries import PathfinderTimeSeries
# from dvl_nav.PathfinderEnsemble import PathfinderEnsemble

# from ds_sensor_msgs.msg import Dvl
# from ds_sensor_msgs.msg import Ranges3D
# from slocum_glider_msgs.msg import Extctl

import rospy
import math

class GliderNavigator(object):
    """Subscribes to DVL Messages and instatiates DVL Python Objects,

Serves as the bridge between ROS and python libraries that compute 
navigation estimates.

    """

    def __init__(self):
        self.depth           = None
        self.temp            = None
        self.cond            = None
        self.pitch           = None
        self.roll            = None
        self.heading         = None

        self.dvl_timestamp   = None
        self.btm_vel_u       = None
        self.btm_vel_v       = None
        self.btm_vel_w       = None

        self.rel_vel_dvl_u   = None
        self.rel_vel_dvl_v   = None
        self.rel_vel_dvl_w   = None

        self.rel_vel_press_u = None
        self.rel_vel_press_v = None
        self.rel_vel_press_w = None
        # self.pd0_raw = None

        self.x_pos = 0
        self.y_pos = 0
        self.init_flag = False

        # CONSTANTS
        self.REL_VEL_BIN_NUM = 0  # which bin will relative thru water velocities be set to (1st bin)

        #Publisher
        self.odom_pub = rospy.Publisher("odom", Odometry, queue_size=50)

        #Subscribers
        self.ctd_sub = rospy.Subscriber(
            '/ctd/data', CTDData, self.handle_ctd_msg
        )
        self.ahrs_sub = rospy.Subscriber(
            '/devices/spartonm2/ahrs', Ahrs, self.handle_ahrs_msg
        )
        self.pd0_sub = rospy.Subscriber(
            '/devices/dvl/pd0', RdiPD0, self.handle_pd0_msg
        )
        # self.pd0_raw_sub = rospy.Subscriber(
        #     '/devices/dvl/instrument/raw', RawData, self.handle_pd0_raw_msg
        # )

    # def handle_pd0_raw_msg(self,msg):
    #     self.pd0_raw = msg.data
    #     print(self.pd0_raw)
    def handle_ctd_msg(self, msg):
        self.depth = msg.depth
        self.temp  = msg.temperature
        self.cond  = msg.conductivity
    def handle_ahrs_msg(self, msg):
        self.pitch   = msg.pitch
        self.roll    = msg.roll
        self.heading = msg.compass.heading
    def handle_pd0_msg(self, msg):
        self.dvl_timestamp = msg.header.stamp.secs

        #TODO
        # Need to rotate, and then u and v velocities sign should be flipped
        if msg.good_beams > 2:
            self.btm_vel_u = msg.velocity[0]
            self.btm_vel_v = msg.velocity[1]
            self.btm_vel_w = msg.velocity[2]
        else:
            self.btm_vel_u = math.nan
            self.btm_vel_v = math.nan
            self.btm_vel_w = math.nan

        #TODO
        # Need to rotate, and then u and v velocities sign should be flipped
        # Check if the bin velocities are valid velocities
        if (msg.bin_velocity_beam1[self.REL_VEL_BIN_NUM] > -32.8) and (msg.bin_velocity_beam1[self.REL_VEL_BIN_NUM] < -32.6) \
            and (msg.bin_velocity_beam2[self.REL_VEL_BIN_NUM] > -32.8) and (msg.bin_velocity_beam2[self.REL_VEL_BIN_NUM] < -32.6) \
                and (msg.bin_velocity_beam3[self.REL_VEL_BIN_NUM] > -32.8) and (msg.bin_velocity_beam3[self.REL_VEL_BIN_NUM] < -32.6):
            self.rel_vel_dvl_u = math.nan
            self.rel_vel_dvl_v = math.nan
            self.rel_vel_dvl_w = math.nan
        else:
            self.rel_vel_dvl_u = msg.bin_velocity_beam1[self.REL_VEL_BIN_NUM]
            self.rel_vel_dvl_v = msg.bin_velocity_beam2[self.REL_VEL_BIN_NUM]
            self.rel_vel_dvl_w = msg.bin_velocity_beam3[self.REL_VEL_BIN_NUM] 


        print(self.btm_vel_u, self.btm_vel_v, self.btm_vel_w)
        print(self.rel_vel_dvl_u,self.rel_vel_dvl_v, self.rel_vel_dvl_w)


    def spin(self):
        rate = rospy.Rate(2)
        try:
            while not rospy.is_shutdown():
                if (self.init_flag == False):
                    prev_time = self.dvl_timestamp
                    prev_x    = 0
                    prev_y    = 0
                    prev_depth = self.depth
                    self.init_flag = True
                    print('Set Initializations')

                if (self.init_flag == True):
                    dt = (self.dvl_timestamp - prev_time)
                    print(dt)
                    delta_z = self.depth - prev_depth
                    print(delta_z)

                    if not math.isnan(self.btm_vel_u):
                        vel_u = self.btm_vel_u
                        vel_v = self.btm_vel_v
                        vel_w = self.btm_vel_w
                    elif not math.isnan(self.rel_vel_dvl_u):
                        vel_u = self.rel_vel_dvl_u
                        vel_v = self.rel_vel_dvl_v
                        vel_w = self.rel_vel_dvl_w
                    
                    delta_x = vel_u*dt
                    delta_y = vel_v*dt

                    x_pos = prev_x + delta_x
                    y_pos = prev_y + delta_y

                    #TODO
                    #check for GPS

                    odom = Odometry()
                    odom.header.stamp = self.dvl_timestamp
                    odom.header.frame_id = "odom"
                    
                    odom.pose.pose = Pose(Point(x_pos, y_pos, self.depth ))

                    self.odom_pub.publish(odom)

                    prev_x = x_pos
                    prev_y = y_pos
                    prev_depth = self.depth
                    prev_time = self.dvl_timestamp


                rospy.spin()
        except:
            print('Aborting!')